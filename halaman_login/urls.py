from django.conf.urls import url
from .views import index,login,registrasi

#url for app
urlpatterns = [
	url(r'^$',index, name='index'),
	url(r'^login/',login, name='login'),
	url(r'^registrasi/',registrasi, name='registrasi')
	# url(r'^halaman_utama_donatur/$',halaman_utama_donatur, name='halaman_utama_donatur')
	# url(r'^halaman_utama_mahasiswa/$',halaman_utama_mahasiswa, name='halaman_utama_mahasiswa')
	# url(r'^halaman_utama_admin/$',halaman_utama_admin, name='halaman_utama_admin')
]
