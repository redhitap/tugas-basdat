from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect, csrf_exempt

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

# Create your views here.
def index(request):
    response ={}
    html = "halaman_login/Halaman_Utama.html"
    return render(request,html,response)

def login(request):
    response ={}
    html = "halaman_login/login.html"
    return render(request,html,response)

def auth(request):
    username = request.POST['username']
    password = request.POST['password']
    cursor=connection.cursor()
    cursor.execute("SELECT * from pengguna where username='"+username+"'")
    hasil=cursor.fetchone();
    if (hasil):
        cursor.execute("SELECT password from pengguna where username='"+username+"'")
        hasil=cursor.fetchone();
        if (hasil[0]==password):
            request.session['username']=username
            cursor.execute("SELECT role from pengguna where username='"+username+"'")
            hasil=cursor.fetchone();
            request.session['role'] = hasil[0]
            if (hasil[0]!="admin"):
                role=hasil[0]
                return HttpResponseRedirect(reverse('Halaman_Utama_Mahasiswa.html'))
                if (role!="mahasiswa"):
                    role="donatur"
                    return HttpResponseRedirect(reverse('Halaman_Utama_Donatur.html'))
                cursor.execute("SELECT nama from " +role+ " where username='"+username+"'")
                hasil=cursor.fetchone();
                request.session['nama']=hasil[0]

            return HttpResponseRedirect(reverse('Halaman_Utama_Admin'))
    messages.error(request, "Username atau password salah")
    return HttpResponseRedirect(reverse('account:login'))




def registrasi(request):
    response ={}
    html = 'halaman_login/Registrasi.html'
    return render(request, html,response)

# def halaman_utama_admin(request):
#     response['status'] = {'status': 'true'}
#     html = 'Halaman_Utama_Admin.html'
#     return render(request, html,response)

# def halaman_utama_donatur(request):
#     response['status'] = {'status': 'true'}
#     html = 'Halaman_Utama_Donatur.html'
#     return render(request, html,response)

# def halaman_utama_mahasiswa(request):
#     response['status'] = {'status': 'true'}
#     html = 'Halaman_Utama_Mahasiswa.html'
#     return render(request, html,response)