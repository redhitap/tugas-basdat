from django.apps import AppConfig


class PendaftaranPaketBeasiswaConfig(AppConfig):
    name = 'pendaftaran_paket_beasiswa'
