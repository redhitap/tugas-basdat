from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages


# Create your views here.
def pendaftaran_beasiswa(request):
    return render(request, 'pendaftaran_paket_beasiswa.html')

def post_form_pendaftaran(request):
    request.session['username'] = 'tunasbangsajkt' #dummy krn belom ada login
    request.session['role'] = 'donatur' #dummy krn belom ada login

    username=request.session['username']
    
    kode = request.POST['kode']
    nama= request.POST['nama_beasiswa']
    jenis= request.POST['jenis']
    deskripsi = request.POST['deskripsi']
    syarat = request.POST['syarat']

    cursor = connection.cursor()
    cursor.execute("SELECT * FROM skema_beasiswa WHERE kode ="+kode)
    result = cursor.fetchone();
    if(result):
        if len(result)>0:
            messages.error(request, "Pendaftaran Gagal: Kode " +kode+" sudah ada")
            return HttpResponseRedirect(reverse('pendaftaran_paket_beasiswa:pendaftaran_beasiswa'))

    

    cursor = connection.cursor()
    cursor.execute("SELECT nomor_identitas from donatur where username='"+username+"'")
    result = cursor.fetchone();
    no_id = result[0];

    
    
    values = "'"+kode+"','"+nama+"','"+jenis+"','"+deskripsi+"', '"+no_id+"'"
    query = "INSERT INTO skema_beasiswa(kode,nama,jenis,deskripsi,nomor_identitas_donatur) values (" + values + ")"
    cursor.execute(query)

    cursor = connection.cursor()
    query = "INSERT INTO syarat_beasiswa (kode_beasiswa,syarat) values ('"+kode+"','"+syarat+"')"
    cursor.execute(query)

    messages.success(request, "Pendaftaran Berhasil")
    return HttpResponseRedirect(reverse('pendaftaran_paket_beasiswa:pendaftaran_beasiswa'))

def tambah_beasiswa(request):
    username=request.session['username']
    cursor = connection.cursor()
    cursor.execute("SELECT skema_beasiswa.kode FROM skema_beasiswa, donatur, pengguna WHERE pengguna.username='"+username+"' AND pengguna.username=donatur.username AND donatur.nomor_identitas = skema_beasiswa.nomor_identitas_donatur")
    result = cursor.fetchall();
    response={}
    response['kodes']=result
    return render(request, 'pendaftaran_paket_beasiswa_2.html', response)

def post_form_tambah(request):
    kode = request.POST['kode']
    no_urut = request.POST['no_urut']
    tgl_mulai = request.POST['tgl_mulai']
    tgl_tutup = request.POST['tgl_tutup']

    cursor = connection.cursor()
    cursor.execute("SELECT * FROM skema_beasiswa_aktif WHERE no_urut ="+no_urut+" AND "+"kode_skema_beasiswa = "+kode)
    result = cursor.fetchone();
    if(result):
        if len(result)>0:
            messages.error(request, "Pendaftaran Gagal: Nomor urut " +no_urut+" sudah ada")
            return HttpResponseRedirect(reverse('pendaftaran_paket_beasiswa:tambah_beasiswa'))

    cursor = connection.cursor()
    values = "'"+kode+"','"+no_urut+"','"+tgl_mulai+"','"+tgl_tutup+"','"+" - "+"','"+"dibuka"+"','"+"0"+"'"
    query = "INSERT INTO skema_beasiswa_aktif (kode_skema_beasiswa, no_urut, tgl_mulai_pendaftaran, tgl_tutup_pendaftaran, periode_penerimaan, status, jumlah_pendaftar) values (" + values + ")"
    cursor.execute(query)

    return HttpResponseRedirect("pendaftaran Berhasil")