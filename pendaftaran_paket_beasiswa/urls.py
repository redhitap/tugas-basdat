from django.conf.urls import url
from .views import *

urlpatterns = [
        url(r'^pendaftaran-beasiswa',pendaftaran_beasiswa, name='pendaftaran_beasiswa'),
		url(r'^post-form-pendaftaran', post_form_pendaftaran, name='post_form_pendaftaran'),
        url(r'^tambah-paket',tambah_beasiswa, name='tambah_beasiswa'),
        url(r'^post-form-tambah', post_form_tambah, name='post_form_tambah'),
]
