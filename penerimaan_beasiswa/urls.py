from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^list-beasiswa/', list_beasiswa, name='list-beasiswa'),
    url(r'^list-pendaftar/', list_pendaftar, name='list_pendaftar'),
]
