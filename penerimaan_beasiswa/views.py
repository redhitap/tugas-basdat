from django.shortcuts import render

# Create your views here.
def list_beasiswa(request):
    return render(request, 'list_beasiswa.html')

def list_pendaftar(request):
    return render(request, 'list_pendaftar_beasiswa.html')