from django.apps import AppConfig


class PenerimaanBeasiswaConfig(AppConfig):
    name = 'penerimaan_beasiswa'
