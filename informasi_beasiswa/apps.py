from django.apps import AppConfig


class InformasiBeasiswaConfig(AppConfig):
    name = 'informasi_beasiswa'
