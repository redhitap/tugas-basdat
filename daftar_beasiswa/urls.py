from django.conf.urls import url
from .views import *

urlpatterns = [
        url(r'^daftar-beasiswa',daftar_beasiswa, name='daftar_beasiswa'),
		url(r'^submit-beasiswa', submit_beasiswa, name='submit_beasiswa')
]
