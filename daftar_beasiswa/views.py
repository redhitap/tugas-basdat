from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.http import HttpResponse

# Create your views here.
def daftar_beasiswa(request,kode_beasiswa=0):
    request.session['username']='oeborn0'
    request.session['role']='mahasiswa'
    response={}
    if ('username' in request.session.keys()):
        username=request.session['username']
        if (request.session['role']=='mahasiswa'):
            cursor=connection.cursor()
            cursor.execute("SELECT npm from mahasiswa where username='"+username+"'")
            result=cursor.fetchone();
            npm = result[0]
            cursor=connection.cursor()
            cursor.execute("SELECT email from mahasiswa where username='"+username+"'")
            result=cursor.fetchone();
            email=result[0]
            cursor=connection.cursor()
            cursor.execute("SELECT kode_skema_beasiswa, nama, no_urut from skema_beasiswa_aktif, skema_beasiswa where kode = kode_skema_beasiswa")
            result=cursor.fetchall();
            kode_beasiswa_aktif = result
            cursor=connection.cursor()
            cursor.execute("SELECT nama from skema_beasiswa where kode = '"+str(kode_beasiswa)+"'")
            result=cursor.fetchone()
            if (result):
                response["nama_kode"]=result[0]
            print(kode_beasiswa_aktif)
            response['npm'] = npm
            response['email'] = email
            response['kode_beasiswa'] = kode_beasiswa
            response['kode_beasiswa_aktif'] = kode_beasiswa_aktif
            return render(request, 'form_pendaftaran.html', response)
    return HttpResponse("Page khusus donatur!")

def submit_beasiswa(request):
    request.session['username']='oeborn0'
    request.session['role']='mahasiswa'
    print(request.POST)
    kode_beasiswa = request.POST['kode_beasiswa']
    print(kode_beasiswa)
    npm = request.POST['npm']
    email = request.POST['email']
    ip = request.POST['ip']

    
    cursor=connection.cursor()
    values =kode_beasiswa+", '"+kode_beasiswa+"', '"+npm+"',CURRENT_TIMESTAMP, 'aktif', 'belum diterima'"
    query = "INSERT INTO pendaftaran(no_urut,kode_skema_beasiswa, npm, waktu_daftar, status_daftar, status_terima) values (" + values + ")"
    cursor.execute(query)

    messages.success(request, "Pendaftaran Berhasil")
    return HttpResponseRedirect(reverse('daftar_beasiswa:daftar_beasiswa'))
