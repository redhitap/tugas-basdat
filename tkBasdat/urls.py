"""tkBasdat URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import halaman_login.urls as halaman_login
import pendaftaran_paket_beasiswa.urls as pendaftaran_paket_beasiswa
import daftar_beasiswa.urls as daftar_beasiswa
import informasi_beasiswa.urls as informasi_beasiswa
import penerimaan_beasiswa.urls as penerimaan_beasiswa

urlpatterns = [
    url(r'^$',include(halaman_login, namespace='index')),
    url(r'^admin/', admin.site.urls),
    url(r'^pendaftaran-beasiswa/', include(pendaftaran_paket_beasiswa, namespace='pendaftaran_paket_beasiswa')),
    url(r'^login/',include(halaman_login, namespace='login')),
    url(r'^registrasi/',include(halaman_login, namespace='registrasi')),
    url(r'^daftar/',include(daftar_beasiswa, namespace='daftar_beasiswa')),
    url(r'^informasi/', include(informasi_beasiswa, namespace='informasi_beasiswa')),
    url(r'^penerimaan/', include(penerimaan_beasiswa, namespace='penerimaan_beasiswa')),

    ]
